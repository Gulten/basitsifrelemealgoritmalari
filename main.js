$(document).ready(function(){

var select_algoritm="";

inserttag = $("#htmltaginsert");

	$("#select_algoritm").on("change", function(){
		select_algoritm = this.value;
		inserttag.empty();
		inserttag.append(eval(select_algoritm).visibletag);
		$("#buton_sifrele").removeAttr('disabled');
		$("#buton_coz").removeAttr('disabled');


	});

	$("#buton_sifrele").on("click", function(){
		eval(select_algoritm).compile($("#acik_metin").val());
	});

	$("#buton_coz").on("click", function(){
		eval(select_algoritm).decompile($("#acik_metin").val());
	});

});

function textboxClear(object){
	$(object + 'input').val("");
	$(object).removeClass("invisible");
}

var ceasar = {
    name: 'ceasar',
    description: 'Ceasar',
    key: '',
    decompkey: '',
    alphabet: ["A", "B", "C", "Ç", "D", "E", "F", "G", "Ğ", "H", "I", "İ", "J", "K", "L", "M", "N", "O", "Ö", "P", "R", "S", "Ş", "T", "U", "Ü", "V", "Y", "Z"],
    visibletag: ['<div class="input-group">',
	    			'<div class="input-group-prepend">',
	    				'<span class="input-group-text">',
	    					'Öteleme Miktarını Giriniz',
	    				'</span>',
	    			'</div>',
	    			'<input type="text" name="kaydir" id="kaydir" value="3">',
	    		'</div>'
	    		].join("\n"),
	compile: function(metin){
		var kaydir = parseInt($("#kaydir").val());
		if (kaydir<0)
			kaydir = 29 + kaydir;

		var yeni_metin = "";
		var index_array = [];
		metin = metin.replace(/\s/g, '');
		var metin_array = metin.split("");
		
		textboxClear(".sifrelenmis-frame");

		for(i=0;i<metin_array.length;i++)
		{
			indis = parseInt($.inArray(metin_array[i], this.alphabet));

			indis = indis + kaydir;
			if (indis>28)
				indis = indis % 28;

			index_array[i] = indis;
		}
		$("#sifrelenmis").val(this.alphabet[index_array[0]]) 

		for(i=1;i<index_array.length;i++)
		{
			$("#sifrelenmis").val($("#sifrelenmis").val() + this.alphabet[index_array[i]]);
		}

	},
	decompile:  function(metin){
		var kaydir = parseInt($("#kaydir").val());
		if (kaydir<0)
			kaydir = 29 + kaydir;
		
		var yeni_metin = "";
		var index_array = [];
		var metin_array = metin.split("");
		
		textboxClear(".sifrelenmis-frame");

		for(i=0;i<metin_array.length;i++)
		{
			indis = parseInt($.inArray(metin_array[i], this.alphabet));
			
			if (kaydir>28)
				kaydir = kaydir % 28

			indis = indis - kaydir;
			if (indis<0)
				indis = 28 + indis;

			index_array[i] = indis;
		}
		$("#sifrelenmis").val(this.alphabet[index_array[0]]) 

		for(i=1;i<index_array.length;i++)
		{
			$("#sifrelenmis").val($("#sifrelenmis").val() + this.alphabet[index_array[i]]);
		}

	}
};

var polybius = {
    name: 'polybius',
    description: 'Polybius',
    key: '',
    decompkey: '',
    visibletag: '<p>Türkçe Kelimeler İçin Şifreleme Yapılacaktır</p>',
    alphabet: [["A", "B", "C", "D", "E"],["F", "G", "Ğ", "H", "I"],["J", "K", "L", "M", "N"],["O", "P", "R", "S", "Ş"],["T", "U", "V", "Y", "Z"]],
    compile: function(metin){
    	var yeni_metin = "";
    	

    	metin = metin.replace(/\Ç/g, "C");
    	metin = metin.replace(/\İ/g, "I");
    	metin = metin.replace(/\Ö/g, "O");
    	metin = metin.replace(/\Ü/g, "U");

    	var metin_array = metin.split("");

    	textboxClear(".sifrelenmis-frame");

		for(i=0;i<metin_array.length;i++){
			for(n=0;n<5;n++){
				for(m=0;m<5;m++){
					if ( metin_array[i] == this.alphabet[n][m] ){
						if(i==0)
							$("#sifrelenmis").val((n+1) + '' + (m+1));
						else
							$("#sifrelenmis").val($("#sifrelenmis").val() + ' ' + (n+1) + (m+1)); 
						break;
					}
				}
			}
		}


    },
    decompile: function(metin){
    	var metin_array = metin.split(" ");
    	for(i=0;i<metin_array.length;i++){
    		buffer = metin_array[i];
    		n = parseInt(buffer.charAt(0)) -1;
    		m = parseInt(buffer.charAt(1)) -1;

    		if(i==0)
				$("#sifrelenmis").val(this.alphabet[n][m]);
			else
				$("#sifrelenmis").val($("#sifrelenmis").val() + this.alphabet[n][m]); 

    	}
    }
};

var columnar = {
	name: "columnar",
	description: 'Sütun (Columnar)',
	key: '',
	decompkey: '',
    visibletag: '<p>Sütun (Columnar) Şifreleme Metodu Kullanılacaktır</p>',
    alphabet: [],
    compile: function(metin){
    	var extra_chars = "ABCÇDEFGĞHIİJKLMNOÖPRSŞTUÜVYZ";
    	var yeni_metin = "";
    	
    	metin = metin.replace(/\s/g, '');

    	var metin_array = metin.split("");

    	var row = Math.ceil(metin_array.length / 5);
    	var random_count = metin_array.length % 5;

    	textboxClear(".sifrelenmis-frame");

		i=0;
		for(n=0;n<row;n++){
			this.alphabet[n] = [];
			for(m=0;m<5;m++){

				if ( i<=(metin_array.length-1) )
					this.alphabet[n][m] = metin_array[i];
				else
					this.alphabet[n][m] = extra_chars.charAt(Math.floor(Math.random() * extra_chars.length));
					i++;
				
			}
		}

		i=0;
		for(m=0;m<5;m++){
			for(n=0;n<row;n++){

				if(i==0)
					$("#sifrelenmis").val(this.alphabet[n][m]);
				else
					$("#sifrelenmis").val($("#sifrelenmis").val() + this.alphabet[n][m]); 
				i++;
			}
			$("#sifrelenmis").val($("#sifrelenmis").val() + " "); 
		}

    },
    decompile: function(metin){
    	var yeni_metin = "";

    	var words = metin.split(" ");
    	columb = words.length-1;//kelime sayısı
    	row = words[0].length;//kelime uzunluğu

    	metin = metin.replace(/\s/g, '');

    	var metin_array = metin.split("");

    	textboxClear(".sifrelenmis-frame");

    	this.alphabet=[];
    	i=0;
    	for(m=0;m<row;m++){
    		this.alphabet[m] = [];
	    	for(n=0;n<columb;n++){
	    		sub_word = words[n].split("");
				this.alphabet[m][n] = sub_word[m];
				i++;
			}	
    	}

		i=0;
		for(m=0;m<row;m++){
			for(n=0;n<columb;n++){
				if(i==0)
					$("#sifrelenmis").val(this.alphabet[m][n]);
				else
					$("#sifrelenmis").val($("#sifrelenmis").val() + this.alphabet[m][n]); 
				i++;
			}
		}
    }


};


var picket = {
	name: "picket",
	description: 'Çit (Picket Fence)',
	key: '',
	decompkey: '',
    visibletag: '<p>Çit (Picket Fence) Şifreleme Metodu Kullanılacaktır</p>',
    alphabet: '',
    compile: function(metin){
    	var yeni_metin = "";
    	var single_chars = "";
    	var double_chars = "";

    	var chars = metin.split("");

		textboxClear(".sifrelenmis-frame");
    	for (n=1;n<=chars.length;n++){
    		if (n%2==0){
    			double_chars += chars[n-1];
    		}
    		else{
    			single_chars += chars[n-1];
    		}
    	}

    	$("#sifrelenmis").val(single_chars + double_chars);
    },
    decompile: function(metin){
    	var chars = metin.split("");
    	var ccount = chars.length;
    	var words = metin.split(" ");
    	
    	textboxClear(".sifrelenmis-frame");

    	this.alphabet="";
    	i=0;
    	j=0;
    	for (n=1;n<=(ccount);n++){

    		if(n%2==0){
    			this.alphabet += chars[(Math.ceil(ccount/2))+j];
    			j++;
	    	}
	    	else{
	    		this.alphabet += chars[i];
	    		i++;
	    	}

    	    

    	}
		$("#sifrelenmis").val(this.alphabet); 
    }

};

var no_single = {
	name: "no_single",
	description: 'Tekil Olmayan Matrisler',
	key: '',
	decompkey: '',
	alphabet: ["A", "B", "C", "Ç", "D", "E", "F", "G", "Ğ", "H", "I", "İ", "J", "K", "L", "M", "N", "O", "Ö", "P", "R", "S", "Ş", "T", "U", "Ü", "V", "Y", "Z"],
	visibletag: '',
    compile: function(metin){
		var yeni_metin = "";
		var index_array = [];
		var metin_array = metin.split("");
		
		textboxClear(".sifrelenmis-frame");

		i=0;
		for (m=0;m<3;m++) {
			index_array[m] = [];
			for(n=0;n<Math.ceil(metin_array.length/3);n++){

				if ((metin_array[i]==" ")||(metin_array[i]==null)){
					indis = 0;
				}
				else{
					
					indis = parseInt($.inArray(metin_array[i], this.alphabet));
					
				}
				
				
				index_array[m][n] = indis;
				i++;
			}
		}

		$("#sifrelenmis").val(index_array); 


    },
    decompile: function(metin){

    }
};